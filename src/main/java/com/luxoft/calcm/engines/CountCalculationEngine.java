package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;

public class CountCalculationEngine extends CalculationEngine {
    private long count;

    @Override
    public Boolean needToProcess(Instrument instrument) {
        return true;
    }

    @Override
    protected void process(Instrument instrument) {

        count++;

    }

    @Override
    public BigDecimal getResult() {
        return BigDecimal.valueOf(count);
    }

    @Override
    public String toString() {
        return "Count: " + getResult();
    }
}
