package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;
import java.util.*;

import static com.luxoft.calcm.Instrument.TYPE.*;

public class NewestSumCalculationEngine extends CalculationEngine {
    private static final Set<String> EXCLUDE = new HashSet<>(Arrays.asList(
            INSTRUMENT1.name(), INSTRUMENT2.name(), INSTRUMENT3.name()));
    private final PriorityQueue<Instrument> pQueue = new PriorityQueue<>(Comparator.comparing(Instrument::getDate));


    @Override
    public Boolean needToProcess(Instrument instrument) {
        return !EXCLUDE.contains(instrument.getName());
    }

    @Override
    protected void process(Instrument instrument) {
        pQueue.add(instrument);
        if (pQueue.size() > 10) {
            pQueue.remove();
        }
    }

    @Override
    public BigDecimal getResult() {

        if (!pQueue.isEmpty()) {
            BigDecimal sum = BigDecimal.ZERO;
            for (Instrument instrument : pQueue) {
                sum = sum.add(instrument.getValue());
            }
            return sum;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "SUM for last 10: " + getResult();
    }
}
