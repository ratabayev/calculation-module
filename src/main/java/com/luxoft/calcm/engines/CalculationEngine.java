package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class CalculationEngine {
    private final Lock lock = new ReentrantLock();

    public abstract Boolean needToProcess(Instrument instrument);

    public final void calculate(Instrument instrument) {

        if (instrument.getName() == null || instrument.getDate() == null || instrument.getValue() == null) {
            throw new IllegalArgumentException("Input params must not be null");
        }

        if (BigDecimal.ZERO.compareTo(instrument.getValue()) >= 0) {
            throw new IllegalArgumentException("Value must be greater than 0");
        }

        if (needToProcess(instrument)) {
            lock.lock();
            try {
                process(instrument);
            } finally {
                lock.unlock();
            }
        }
    }

    protected abstract void process(Instrument instrument);


    public abstract BigDecimal getResult();
}
