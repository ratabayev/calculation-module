package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;

public class MaxCalculationEngine extends CalculationEngine {
    private static final Instrument.TYPE INSTRUMENT = Instrument.TYPE.INSTRUMENT3;
    private BigDecimal currentMaxValue;

    @Override
    public Boolean needToProcess(Instrument instrument) {
        return INSTRUMENT.name().equals(instrument.getName());
    }

    @Override
    protected void process(Instrument instrument) {

        if (currentMaxValue == null) {
            currentMaxValue = instrument.getValue();
        } else {
            if (currentMaxValue.compareTo(instrument.getValue()) < 0) {
                currentMaxValue = instrument.getValue();
            }
        }

    }

    @Override
    public BigDecimal getResult() {
        return currentMaxValue;
    }

    @Override
    public String toString() {
        return "MAX for " + INSTRUMENT.name() + ": " + getResult();
    }
}
