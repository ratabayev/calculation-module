package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.time.YearMonth;

public class MonthYearMeanCalculationEngine extends MeanCalculationEngine {
    private static final Instrument.TYPE INSTRUMENT = Instrument.TYPE.INSTRUMENT2;
    private final YearMonth yearMonth;

    public MonthYearMeanCalculationEngine(YearMonth yearMonth) {
        this.yearMonth = yearMonth;
    }

    @Override
    public Boolean needToProcess(Instrument instrument) {
        return INSTRUMENT.name().equals(instrument.getName()) && yearMonth.equals(YearMonth.from(instrument.getDate()));
    }

    @Override
    public String toString() {
        return "MEAN for " + yearMonth + " " + INSTRUMENT.name() + ": " + getResult();
    }
}
