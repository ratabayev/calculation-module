package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;

public class MeanCalculationEngine extends CalculationEngine {
    private static final Instrument.TYPE INSTRUMENT = Instrument.TYPE.INSTRUMENT1;
    private BigDecimal sum = BigDecimal.ZERO;
    private long count;

    @Override
    public Boolean needToProcess(Instrument instrument) {
        return INSTRUMENT.name().equals(instrument.getName());
    }

    @Override
    protected void process(Instrument instrument) {

        count++;
        sum = sum.add(instrument.getValue());

    }

    @Override
    public BigDecimal getResult() {

        if (count > 0) {
            return sum.divide(BigDecimal.valueOf(count), sum.scale(), BigDecimal.ROUND_HALF_UP);
        } else {
            return null;
        }

    }

    @Override
    public String toString() {
        return "MEAN for " + INSTRUMENT.name() + ": " + getResult();
    }
}
