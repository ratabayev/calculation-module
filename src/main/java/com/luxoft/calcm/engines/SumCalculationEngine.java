package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;

import java.math.BigDecimal;

public class SumCalculationEngine extends CalculationEngine {
    private BigDecimal sum = BigDecimal.ZERO;

    @Override
    public Boolean needToProcess(Instrument instrument) {
        return true;
    }

    @Override
    protected void process(Instrument instrument) {

        sum = sum.add(instrument.getValue());

    }

    @Override
    public BigDecimal getResult() {
        return sum;
    }

    @Override
    public String toString() {
        return "SUM: " + getResult();
    }
}
