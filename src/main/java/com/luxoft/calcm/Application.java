package com.luxoft.calcm;

import com.luxoft.calcm.engines.MaxCalculationEngine;
import com.luxoft.calcm.engines.MeanCalculationEngine;
import com.luxoft.calcm.engines.MonthYearMeanCalculationEngine;
import com.luxoft.calcm.engines.NewestSumCalculationEngine;
import com.luxoft.calcm.entities.InstrumentPriceModifier;
import com.luxoft.calcm.repositories.InstrumentPriceModifierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.time.Month;
import java.time.YearMonth;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private InstrumentPriceModifierRepository instrumentPriceModifierRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        if (args.length < 2) {
            throw new IllegalArgumentException("Batch size and path to file with data are required");
        }

        generateInstrumentPriceModifiersForTest();

        Map<String, BigDecimal> modifiers = instrumentPriceModifierRepository
                .findAll()
                .parallelStream()
                .collect(Collectors.toMap(InstrumentPriceModifier::getName, InstrumentPriceModifier::getMultiplier));

        CalculationModule calculationModule = new CalculationModule(
                Integer.parseInt(args[0]), // batch size
                modifiers,
                new MaxCalculationEngine(),
                new MeanCalculationEngine(),
                new MonthYearMeanCalculationEngine(YearMonth.of(2014, Month.NOVEMBER)),
                new NewestSumCalculationEngine()/*,
                new CountCalculationEngine(), // I've used this class for testing
                new SumCalculationEngine() // I've used this class for testing*/
        );

        calculationModule.calculate(args[1]);

        calculationModule.printResult();

    }

    private void generateInstrumentPriceModifiersForTest() {
        for (int i = 1; i <= 3; i++) {
            InstrumentPriceModifier inst = InstrumentPriceModifier
                    .builder()
                    .name("INSTRUMENT" + i)
                    .multiplier(BigDecimal.valueOf(1))
                    .build();

            instrumentPriceModifierRepository.save(inst);
        }
    }
}
