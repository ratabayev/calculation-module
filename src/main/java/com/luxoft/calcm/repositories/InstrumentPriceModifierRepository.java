package com.luxoft.calcm.repositories;

import com.luxoft.calcm.entities.InstrumentPriceModifier;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InstrumentPriceModifierRepository extends CrudRepository<InstrumentPriceModifier, Long> {
    List<InstrumentPriceModifier> findAll();
}
