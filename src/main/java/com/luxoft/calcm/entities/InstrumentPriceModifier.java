package com.luxoft.calcm.entities;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@Entity
@Table(name = "INSTRUMENT_PRICE_MODIFIER")
public class InstrumentPriceModifier {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;
    @Column(name = "NAME", length = 64)
    private String name;
    @Column(name = "MULTIPLIER")
    private BigDecimal multiplier;

}
