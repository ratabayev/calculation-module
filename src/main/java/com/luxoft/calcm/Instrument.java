package com.luxoft.calcm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class Instrument {

    public enum TYPE {INSTRUMENT1, INSTRUMENT2, INSTRUMENT3}

    private final String name;
    private final LocalDate date;
    private final BigDecimal value;
}
