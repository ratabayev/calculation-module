package com.luxoft.calcm;

import com.luxoft.calcm.engines.CalculationEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.*;

public class CalculationModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationModule.class);
    private final int batchSize;
    private final int threadCount;
    private final CalculationEngine[] calculationEngines;
    private final Map<String, BigDecimal> modifiers;
    private final ThreadPoolExecutor threadPoolExecutor;


    public CalculationModule(int batchSize, Map<String, BigDecimal> modifiers,
                             CalculationEngine... calculationEngines) {
        this.batchSize = Math.max(1, batchSize);
        this.threadCount = Runtime.getRuntime().availableProcessors();
        this.modifiers = modifiers;
        this.calculationEngines = calculationEngines;
        this.threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(this.threadCount);
    }

    public void calculate(String filePath) throws IOException, InterruptedException, ExecutionException {
        try (
                BufferedReader reader = Files.newBufferedReader(Paths.get(filePath))
        ) {
            calculate(reader);
        }
    }

    public void calculate(BufferedReader reader) throws IOException, InterruptedException, ExecutionException {

        if (calculationEngines.length > 0) {

            Queue<Future<Void>> futures = new LinkedList<>();

            while (true) {
                String[] lines = readBatch(reader);

                // nothing to process
                // As I've decided to use fixed array instead of ArrayList, so I need to use this check
                if (lines[0] == null) {
                    break;
                }

                futures.add(startProcessBatch(lines));

                // In order not to exceed memory
                if (futures.size() == threadCount) {
                    futures.remove().get();
                }
            }

            // It's required to wait last futures
            for (Future<Void> future : futures) {
                future.get();
            }

        }

        threadPoolExecutor.shutdown();

    }

    private String[] readBatch(BufferedReader reader) throws IOException {
        String[] lines = new String[batchSize];
        int currentLine = 0;
        String thisLine;
        while (currentLine < batchSize && (thisLine = reader.readLine()) != null) {
            lines[currentLine++] = thisLine;
        }
        return lines;
    }

    private Future<Void> startProcessBatch(String[] lines) {
        return CompletableFuture.runAsync(() -> {
            for (String data : lines) {
                if (data == null) {
                    break;
                }
                // In order to parse CSV file, at the beginning I tried to use org.apache.commons commons-csv,
                // but performance was much worse
                String[] arr = data.split(",");
                String name = arr[0];
                String date = arr[1];
                String value = arr[2];

                final Instrument instrument = Instrument.builder()
                        .name(name)
                        .date(LocalDate.parse(date, DateTimeFormatter.ofPattern("d-MMM-yyyy")))
                        .value(new BigDecimal(value).multiply(modifiers.getOrDefault(name, BigDecimal.ONE)))
                        .build();


                processInstruments(instrument);
            }
        }, threadPoolExecutor);
    }

    private void processInstruments(Instrument instrument) {
        for (CalculationEngine cm : calculationEngines) {
            cm.calculate(instrument);
        }
    }

    public void printResult() {
        LOGGER.info("/**************************/");
        for (CalculationEngine cm : calculationEngines) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(cm.toString());
            }
        }
        LOGGER.info("/**************************/");
    }
}
