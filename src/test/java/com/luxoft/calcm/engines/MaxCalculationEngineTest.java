package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class MaxCalculationEngineTest {

    @Test
    public void testMaxCalculationModule() {
        CalculationEngine ce = new MaxCalculationEngine();
        assertNull("Must be null for new instance", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT1", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNotNull("Must be not null for INSTRUMENT3", ce.getResult());
        assertEquals("Must become 3.3", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT2 mustn't affect", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT3 must affect", BigDecimal.valueOf(4.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(2.3)).build());
        assertEquals("lower value mustn't affect", BigDecimal.valueOf(4.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(5.3)).build());
        assertEquals("higher value must affect", BigDecimal.valueOf(5.3), ce.getResult());

    }

    @Test
    public void testToString() {
        CalculationEngine ce = new MaxCalculationEngine();
        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("MAX for INSTRUMENT3: 3.3", ce.toString());
    }

}
