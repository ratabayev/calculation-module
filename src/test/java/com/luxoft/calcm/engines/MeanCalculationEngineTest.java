package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class MeanCalculationEngineTest {

    @Test
    public void testMeanCalculationModule() {
        CalculationEngine ce = new MeanCalculationEngine();
        assertNull("Must be null for new instance", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT2", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNotNull("Must be not null for INSTRUMENT1", ce.getResult());
        assertEquals("Must become 3.3", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT2 mustn't affect", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT1 must affect", BigDecimal.valueOf(3.8), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(2.3)).build());
        assertEquals("any value must affect", BigDecimal.valueOf(3.3), ce.getResult());
    }

    @Test
    public void testToString() {
        CalculationEngine ce = new MeanCalculationEngine();
        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("MEAN for INSTRUMENT1: 3.3", ce.toString());
    }

}
