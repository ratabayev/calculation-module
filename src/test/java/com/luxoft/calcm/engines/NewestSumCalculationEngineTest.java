package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class NewestSumCalculationEngineTest {

    @Test
    public void testNewestSumCalculationModule() {
        CalculationEngine ce = new NewestSumCalculationEngine();
        assertNull("Must be null for new instance", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT1", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT2", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT3", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT4").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNotNull("Must be not null for INSTRUMENT4", ce.getResult());
        assertEquals("Must become 3.3", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT2 mustn't affect", BigDecimal.valueOf(3.3), ce.getResult());


        ce.calculate(Instrument
                .builder().name("INSTRUMENT4").date(LocalDate.now().plusDays(1)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT5").date(LocalDate.now().plusDays(2)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT6").date(LocalDate.now().plusDays(3)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT7").date(LocalDate.now().plusDays(4)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT8").date(LocalDate.now().plusDays(5)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT9").date(LocalDate.now().plusDays(6)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT10").date(LocalDate.now().plusDays(7)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT11").date(LocalDate.now().plusDays(8)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT12").date(LocalDate.now().plusDays(9)).value(BigDecimal.valueOf(1)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now().plusDays(9)).value(BigDecimal.valueOf(100)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now().plusDays(9)).value(BigDecimal.valueOf(100)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now().plusDays(9)).value(BigDecimal.valueOf(100)).build());
        ce.calculate(Instrument
                .builder().name("INSTRUMENT13").date(LocalDate.now().plusDays(10)).value(BigDecimal.valueOf(1)).build());
        assertEquals("sum for last 10 days must be 10", BigDecimal.valueOf(10), ce.getResult());


    }

    @Test
    public void testToString() {
        CalculationEngine ce = new NewestSumCalculationEngine();
        ce.calculate(Instrument
                .builder().name("INSTRUMENT13").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("SUM for last 10: 3.3", ce.toString());
    }

}
