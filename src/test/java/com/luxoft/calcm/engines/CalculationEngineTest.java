package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.mockito.Mockito.mock;

public class CalculationEngineTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNullParams() {
        CalculationEngine ce = mock(CalculationEngine.class);

        ce.calculate(Instrument.builder().build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeValue() {
        CalculationEngine ce = mock(CalculationEngine.class);

        ce.calculate(Instrument.builder().name("ANY").date(LocalDate.now()).value(BigDecimal.valueOf(-1)).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZerValue() {
        CalculationEngine ce = mock(CalculationEngine.class);

        ce.calculate(Instrument.builder().name("ANY").date(LocalDate.now()).value(BigDecimal.ZERO).build());
    }
}
