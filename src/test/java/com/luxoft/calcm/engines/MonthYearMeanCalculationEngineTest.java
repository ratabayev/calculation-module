package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;

import static org.junit.Assert.*;

public class MonthYearMeanCalculationEngineTest {

    @Test
    public void testMonthYearMeanCalculationModule() {
        CalculationEngine ce = new MonthYearMeanCalculationEngine(YearMonth.from(LocalDate.now()));
        assertNull("Must be null for new instance", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNull("Must be null for INSTRUMENT1", ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertNotNull("Must be not null for INSTRUMENT2", ce.getResult());
        assertEquals("Must become 3.3", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT1 mustn't affect", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(4.3)).build());
        assertEquals("INSTRUMENT2 must affect", BigDecimal.valueOf(3.8), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(2.3)).build());
        assertEquals("any value must affect", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now().minusMonths(1)).value(BigDecimal.valueOf(2.3)).build());
        assertEquals("Only specific month must affect", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now().minusYears(1)).value(BigDecimal.valueOf(2.3)).build());
        assertEquals("Only specific year must affect", BigDecimal.valueOf(3.3), ce.getResult());


    }

    @Test
    public void testToString() {
        CalculationEngine ce = new MonthYearMeanCalculationEngine(YearMonth.from(LocalDate.now()));
        ce.calculate(Instrument
                .builder().name("INSTRUMENT2").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("MEAN for " + YearMonth.from(LocalDate.now()) + " INSTRUMENT2: 3.3", ce.toString());
    }
}
