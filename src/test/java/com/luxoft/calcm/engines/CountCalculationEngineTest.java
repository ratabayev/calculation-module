package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class CountCalculationEngineTest {

    @Test
    public void testCountCalculationModule() {
        CalculationEngine ce = new CountCalculationEngine();
        assertEquals("Must be 0 for new instance", BigDecimal.ZERO, ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("Must be 1", BigDecimal.ONE, ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("Must be 2", BigDecimal.valueOf(2), ce.getResult());
    }

    @Test
    public void testToString() {
        CalculationEngine ce = new CountCalculationEngine();
        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("Count: 1", ce.toString());
    }
}
