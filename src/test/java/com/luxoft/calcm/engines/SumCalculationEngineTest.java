package com.luxoft.calcm.engines;

import com.luxoft.calcm.Instrument;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class SumCalculationEngineTest {

    @Test
    public void testSumCalculationModule() {
        CalculationEngine ce = new SumCalculationEngine();
        assertEquals("Must be 0 for new instance", BigDecimal.ZERO, ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT1").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("Must be 3.3", BigDecimal.valueOf(3.3), ce.getResult());

        ce.calculate(Instrument
                .builder().name("INSTRUMENT3").date(LocalDate.now()).value(BigDecimal.valueOf(4.4)).build());
        assertEquals("Must be 7.7", BigDecimal.valueOf(7.7), ce.getResult());

    }

    @Test
    public void testToString() {
        CalculationEngine ce = new SumCalculationEngine();
        ce.calculate(Instrument
                .builder().name("INSTRUMENT13").date(LocalDate.now()).value(BigDecimal.valueOf(3.3)).build());
        assertEquals("SUM: 3.3", ce.toString());
    }

}
