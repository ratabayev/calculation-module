package com.luxoft.calcm;

import com.luxoft.calcm.engines.CountCalculationEngine;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;

public class CalculationModuleTest {

    @Test
    public void testCalculationModule() throws InterruptedException, ExecutionException, IOException {
        int batchSize = 1;
        Map<String, BigDecimal> modifiers = new HashMap<>();
        CountCalculationEngine ce = new CountCalculationEngine();
        CalculationModule calculationModule = new CalculationModule(batchSize, modifiers, ce);

        String str = "INSTRUMENT1,21-Feb-1996,2.537\n" +
                "INSTRUMENT1,22-Feb-1996,2.54\n" +
                "INSTRUMENT1,23-Feb-1996,2.549\n" +
                "INSTRUMENT1,26-Feb-1996,2.538\n" +
                "INSTRUMENT1,27-Feb-1996,2.543";
        BufferedReader reader = new BufferedReader(new StringReader(str));

        calculationModule.calculate(reader);

        assertEquals("Count must be 5", BigDecimal.valueOf(5), ce.getResult());


    }
}
