package com.luxoft.calcm;

import com.luxoft.calcm.engines.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculationEngineTest.class,
        CountCalculationEngineTest.class,
        MaxCalculationEngineTest.class,
        MeanCalculationEngineTest.class,
        MonthYearMeanCalculationEngineTest.class,
        NewestSumCalculationEngineTest.class,
        SumCalculationEngineTest.class,
        CalculationModuleTest.class
})

public class CMTestSuite {
}
